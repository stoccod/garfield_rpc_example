# RPC Simulation Examples with Visualization

## Build Project
To build the examples you will need to assign in the CMakeLists.txt the paths to your Garfield and ROOT 6 packages:
```bash
set(CMAKE_PREFIX_PATH <path_to_garfield/install> : <path_to_ROOT6/root>)
```

Then you can go (Linux Terminal) into the /build folder (cd /build) and build and run it by:
```bash
cmake .. && make && ./RPC
```


## Description

In rpc_signal the development of avalanches (from crossing pion) in a 2mm RPC gap is showed on the readout-strip=anode.

In rpc_avalanche_plot a two-dimensional view on the development of the avalanche is plotted. 

In rpc_performance the evaluation of induced charge, potential streamer formation and
time of crossing a certain induced current threshold is calculated using Garfield++ from the passage of a pion.