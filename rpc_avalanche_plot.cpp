//
// Created by stoccod on 21.04.2023.
//
#include <iostream>
#include <fstream>
#include <cstdlib>




#include <TCanvas.h>
#include <TROOT.h>
#include <TApplication.h>
#include <TH1D.h>
#include <TH1.h>
#include <TSystem.h>


#include "Garfield/ViewCell.hh"
#include "Garfield/ViewDrift.hh"
#include "Garfield/ViewSignal.hh"

#include "Garfield/ComponentParallelPlate.hh"
#include "Garfield/SolidBox.hh"
#include "Garfield/GeometrySimple.hh"
#include "Garfield/AvalancheGrid.hh"
#include "Garfield/Plotting.hh"
#include "Garfield/FundamentalConstants.hh"
#include "Garfield/AvalancheMicroscopic.hh"

#include "Garfield/ComponentAnalyticField.hh"
#include "Garfield/MediumMagboltz.hh"
#include "Garfield/ViewMedium.hh"
#include "Garfield/Sensor.hh"
#include "Garfield/DriftLineRKF.hh"
#include "Garfield/TrackHeed.hh"

using namespace Garfield;
int main(int argc, char * argv[]) {
    bool plot_avalanche = true;
    bool debug_mode = true;

    // ROOT application, needs to be initialized otherwise it does not show any plots or canvas
    TApplication app("app", &argc, argv);
    plottingEngine.SetDefaultStyle();


    // SETUP GAS MEDIUM
    MediumMagboltz gas;
    gas.LoadGasFile("ar_93_co2_7.gas");
    gas.Initialise(true);


    // SETUP RPC
    std::vector<double> d = {0.2, 0.2, 0.2};  // [cm], z = [0, 0.2] U [0.2, 0.4] U [0.4, 0.6] style
    std::vector<double> eps = {8., 1., 8.};  // [1]
    const double voltage = -9500;  // [V]
    // resistive thus conducting electrodes (voltage on their surface): layer 1 and 3 (2 is gas phase)
    std::vector<int> sigma = {1, 3}; // [S/m]

    auto *rpc = new ComponentParallelPlate();
    rpc->EnableDebugging();
    rpc->Setup(3, eps, d, voltage, sigma);
    rpc->SetMedium(&gas); // how to adjust the epsilon for the gas gap?

    // box containing the gas gap (it should be y [0.2, 0.4] = 0.3 \pm 0.1)
    // this medium defines the gas region used in sensor
    SolidBox box(0, 0.3, 0, 5, 0.1, 5);
    GeometrySimple geo;
    geo.AddSolid(&box, &gas);
    rpc->SetGeometry(&geo);

    if (debug_mode) {
        double exx, eyy, ezz;
        int status;
        Medium *m;
        // y axis is drift axis
        rpc->ElectricField(0, 0.3, 0, exx, eyy, ezz, m, status);
        std::cout << "Electric field (xyz): " << exx << " " << eyy << " " << ezz << std::endl;
    }

    // Adding a readout structure.
    const std::string label = "readout_plane";
    // adds a readout plane at the anode of the Parallel Plate (y = 0)
    rpc->AddPlane(label);

    // Create the sensor.
    Sensor sensor;
    sensor.AddComponent(rpc);
    sensor.AddElectrode(rpc, label);
    // Set the time bins.
    const unsigned int nTimeBins = 150;
    const double tmin = 0.;
    const double tmax = 25; // typical time of signal in [ns]
    const double tstep = (tmax - tmin) / nTimeBins;
    sensor.SetTimeWindow(tmin, tstep, nTimeBins);
    // FINISH SETUP RPC


    // SETUP AVALANCHE PROPAGATION
    // the avalanche grid - style of avalanche propagation is based on Legler's Model?
    AvalancheGrid avalgrid;
    // where to set the grid i.e. from xmin to xmax (same for y) and z min and zmax should coincide with the gas gap
    avalgrid.SetGrid(-0.05, 0.05, 500, 0.2, 0.4, 500, -0.05, 0.05, 500);
    avalgrid.SetSensor(&sensor);

    AvalancheMicroscopic aval;
    // set weighting potential of rpc to avalanchemicroscopic
    aval.UseWeightingPotential();
    aval.SetSensor(&sensor);
    // aval.EnableDebugging();

    // Set time window where the calculations will be done microscopically
    const double tMaxWindow = 2; //  2ns stochastic regime?
    aval.SetTimeWindow(0., tMaxWindow);
    avalgrid.SetAvalancheMicroscopic(&aval);
    // FINISH SETUP AVALANCHE PROPAGATION


    // Set up pion track via HEED
    TrackHeed track;
    track.SetSensor(&sensor);
    track.SetParticle("pion");
    track.SetMomentum(1.e11);


    // prepare plotting of drift lines of electrons
    ViewDrift *drift_view = nullptr;
    TCanvas *canvas_drift = nullptr;
    if (plot_avalanche) {
        canvas_drift = new TCanvas("cSignal", "", 600, 600);
        drift_view = new ViewDrift();
        drift_view->SetCanvas(canvas_drift);
        // enable track and avalanche to be plotted
        track.EnablePlotting(drift_view);
        aval.EnablePlotting(drift_view, 10);
    }

    // START AVALANCHE
    // Simulate pion track
    // this points needs to be in the medium box
    track.NewTrack(0., 0.35, 0., 0., 0., -1, 0);

    double xc = 0., yc = 0., zc = 0., tc = 0., ec = 0., extra = 0.;
    int ne = 0;
    // Retrieve the clusters along the track.
    while (track.GetCluster(xc, yc, zc, tc, ne, ec, extra)) {
        // Loop over the electrons in the cluster.
        if (debug_mode) {
            std::cout << "number of electrons in cluster: " << ne << std::endl;
        }
        for (int j = 0; j < ne; ++j) {
            double xe = 0., ye = 0., ze = 0., te = 0., ee = 0.;
            double dxe = 0., dye = 0., dze = 0.;
            track.GetElectron(j, xe, ye, ze, te, ee, dxe, dye, dze);
            // Propagate avalanche though gap within the time window set (using some collision time MC model (Schindler Thesis?))
            aval.AvalancheElectron(xe, ye, ze, te, ee, dxe, dye, dze);

            if (debug_mode) {
                int nee, nie;
                aval.GetAvalancheSize(nee, nie);
                std::cout << "AvalancheSize after 2ns: " << nee << std::endl;
            }
            // Stops calculation after tMaxWindow ns
            // add electrons to avalgrid
            avalgrid.GetElectronsFromAvalancheMicroscopic();
        }
    }

    // Start grid based avalanche calculations starting from where the microsocpic
    // calculations stoped
    // this propagates using Legler's (61) model
    avalgrid.StartGridAvalanche();
    // FINISH AVALANCHE

    // plot result
    if (plot_avalanche) {
        // TODO: avalanche seem to stop (no full avalanche-picture to the resistive layer), why?
        // set area within plane (TODO test which is x and y)
        // drift_view->SetArea(-0.1, 0.2, -0.1, 0.1, 0.4, 0.1);
        // set plane within geometries
        // drift_view->SetPlane(0, 0, 1, 0, 0.3, 0);
        constexpr bool twod = true;
        drift_view->Plot(twod, true);
        canvas_drift->Update();
        gSystem->ProcessEvents();
    }


    app.Run(kTRUE);
    return 0;
}