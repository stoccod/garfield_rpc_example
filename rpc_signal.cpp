//
// Created by stoccod on 21.04.2023.
//
#include <iostream>
#include <fstream>
#include <cstdlib>


#include <TCanvas.h>
#include <TROOT.h>
#include <TApplication.h>
#include <TH1D.h>
#include <TH1.h>
#include <TSystem.h>


#include "Garfield/ViewCell.hh"
#include "Garfield/ViewDrift.hh"
#include "Garfield/ViewSignal.hh"

#include "Garfield/ComponentParallelPlate.hh"
#include "Garfield/SolidBox.hh"
#include "Garfield/GeometrySimple.hh"
#include "Garfield/AvalancheGrid.hh"
#include "Garfield/Plotting.hh"
#include "Garfield/FundamentalConstants.hh"
#include "Garfield/AvalancheMicroscopic.hh"

#include "Garfield/ComponentAnalyticField.hh"
#include "Garfield/MediumMagboltz.hh"
#include "Garfield/ViewMedium.hh"
#include "Garfield/Sensor.hh"
#include "Garfield/DriftLineRKF.hh"
#include "Garfield/TrackHeed.hh"

using namespace Garfield;


int main(int argc, char * argv[]){
    bool plot_signal = true;
    bool debug_mode = true;

    // ROOT application, needs to be initialized otherwise it does not show any plots or canvas
    TApplication app("app", &argc, argv);
    plottingEngine.SetDefaultStyle();

    // gas medium
    MediumMagboltz gas;
    gas.LoadGasFile("ar_93_co2_7.gas");
    gas.Initialise(true);

    // The geometry of the RPC
    std::vector<double> d = {0.2, 0.2, 0.2};  // [cm], z = [0, 0.2] U [0.2, 0.4] U [0.4, 0.6] style
    std::vector<double> eps = {8., 1., 8.};  // [1]
    const double voltage = -9500;  // [V]
    // resistive thus conducting electrodes (voltage on their surface): layer 1 and 3 (2 is gas phase)
    std::vector<int> sigma = {1, 3}; // [S/m]

    auto* rpc = new ComponentParallelPlate();
    rpc->EnableDebugging();
    rpc->Setup(3, eps, d, voltage, sigma);
    rpc->SetMedium(&gas); // how to adjust the epsilon for the gas gap?

    // box containing the gas gap (it should be y [0.2, 0.4])
    // this medium defines the gas region used in sensor
    SolidBox box(0, 0.3, 0, 5, 0.1, 5);
    GeometrySimple geo;
    geo.AddSolid(&box, &gas);
    rpc->SetGeometry(&geo);

    if(debug_mode){
        double exx, eyy, ezz;
        int status;
        Medium* m;
        // y axis is drift axis
        rpc->ElectricField(0, 0.3, 0, exx, eyy, ezz, m, status);
        std::cout << "Electric field (xyz): " << exx << " " << eyy << " " << ezz << std::endl;
    }

    // Adding a readout structure.
    const std::string label = "readout_plane";
    // adds a readout plane at the anode of the Parallel Plate (y = 0)
    rpc->AddPlane(label);

    // Create the sensor.
    Sensor sensor;
    sensor.AddComponent(rpc);
    sensor.AddElectrode(rpc, label);
    // Set the time bins.
    const unsigned int nTimeBins = 150;
    const double tmin = 0.;
    const double tmax = 25; // typical time of signal in [ns]
    const double tstep = (tmax - tmin) / nTimeBins;
    sensor.SetTimeWindow(tmin, tstep, nTimeBins);

    // prepare plotting of induced signal
    ViewSignal *signalView = nullptr;
    TCanvas *cSignal = nullptr;
    if(plot_signal) {
        cSignal = new TCanvas("cSignal", "", 600, 600);
        signalView = new ViewSignal();
        signalView->SetCanvas(cSignal);
        signalView->SetSensor(&sensor);
    }

    // Create the AvalancheGrid for grid-based avalanche calculations
    // the avalanche grid - style of avalanche propagation is based on Legler's Model?
    AvalancheGrid avalgrid;
    // where to set the grid i.e. from xmin to xmax (same for y) and z min and zmax should coincide with the gas gap
    avalgrid.SetGrid(-0.05, 0.05, 500, 0.2, 0.4, 500, -0.05, 0.05, 500);
    avalgrid.SetSensor(&sensor);

    AvalancheMicroscopic aval;
    aval.UseWeightingPotential();
    aval.SetSensor(&sensor);
    // aval.EnableDebugging();

    // Set time window where the calculations will be done microscopically
    const double tMaxWindow =  2; //  2ns stochastic regime?
    aval.SetTimeWindow(0., tMaxWindow);
    avalgrid.SetAvalancheMicroscopic(&aval);


    // Set up pion track via HEED
    TrackHeed track;
    track.SetSensor(&sensor);
    track.SetParticle("pion");
    track.SetMomentum(1.e11);


    // START AVALANCHE
    // Simulate pion track
    // this points needs to be in the medium box
    track.NewTrack(0., 0.35, 0., 0., 0., -1, 0);

    double xc = 0., yc = 0., zc = 0., tc = 0., ec = 0., extra = 0.;
    int ne = 0;
    // Retrieve the clusters along the track.
    while (track.GetCluster(xc, yc, zc, tc, ne, ec, extra)) {
        // Loop over the electrons in the cluster.
        if(debug_mode) {
            std::cout << "number of electrons in cluster: " << ne << std::endl;
        }
        for (int j = 0; j < ne; ++j) {
            double xe = 0., ye = 0., ze = 0., te = 0., ee = 0.;
            double dxe = 0., dye = 0., dze = 0.;
            track.GetElectron(j, xe, ye, ze, te, ee, dxe, dye, dze);
            // Propagate avalanche though gap within the time window set (using some collision time MC model (Schindler Thesis?))
            aval.AvalancheElectron(xe, ye, ze, te, ee, dxe, dye, dze);

            if(debug_mode){
                int nee, nie;
                aval.GetAvalancheSize(nee, nie);
                std::cout << "AvalancheSize after 2ns: " <<nee << std::endl;
            }
            // Stops calculation after tMaxWindow ns
            // add electrons to avalgrid
            avalgrid.GetElectronsFromAvalancheMicroscopic();
        }
    }

    // Start grid based avalanche calculations starting from where the microsocpic
    // calculations stoped
    // this propagates using Legler's (61) model
    avalgrid.StartGridAvalanche();
    // FINISH AVALANCHE

    // plot result

    // Plot signals
    if(plot_signal){
        signalView->PlotSignal(label);
        cSignal->Update();
        gSystem->ProcessEvents();

        sensor.ExportSignal(label,"SignalNaN");
    }


    app.Run(kTRUE);
    return 0;
}