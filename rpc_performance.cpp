//
// Created by stoccod on 21.04.2023.
//
#include <iostream>
#include <fstream>
#include <cstdlib>


#include <TCanvas.h>
#include <TROOT.h>
#include <TApplication.h>
#include <TH1D.h>
#include <TH1.h>
#include <TSystem.h>


#include "Garfield/ViewCell.hh"
#include "Garfield/ViewDrift.hh"
#include "Garfield/ViewSignal.hh"

#include "Garfield/ComponentParallelPlate.hh"
#include "Garfield/SolidBox.hh"
#include "Garfield/GeometrySimple.hh"
#include "Garfield/AvalancheGrid.hh"
#include "Garfield/Plotting.hh"
#include "Garfield/FundamentalConstants.hh"
#include "Garfield/AvalancheMicroscopic.hh"

#include "Garfield/ComponentAnalyticField.hh"
#include "Garfield/MediumMagboltz.hh"
#include "Garfield/ViewMedium.hh"
#include "Garfield/Sensor.hh"
#include "Garfield/DriftLineRKF.hh"
#include "Garfield/TrackHeed.hh"

using namespace Garfield;

void single_passage(Sensor &sensor, double &q_ind, double &t_cross, bool &streamer_flag, const std::string &label, unsigned int last_bin,
                    const double current_threshold, std::vector<double> rpc_size, bool debug_mode=false){
    // Set up pion track via HEED
    TrackHeed track;
    track.SetSensor(&sensor);
    track.SetParticle("pion");
    track.SetMomentum(1.e11);

    // Simulate pion track
    // this points needs to be in the medium box
    track.NewTrack(0., rpc_size[0] + rpc_size[1] - 1e-5, 0., 0., 0., -1, 0);

    // Create the AvalancheGrid for grid-based avalanche calculations
    // the avalanche grid - style of avalanche propagation is based on Legler's Model?
    AvalancheGrid avalgrid;
    // where to set the grid i.e. the gas gap dimension are y_min/y_may = [0.2, 0.4] and in x and z direction no bounds
    avalgrid.SetGrid(-0.05, 0.05, 500, rpc_size[0], rpc_size[0] + rpc_size[1], 500, -0.05, 0.05, 500);
    avalgrid.SetSensor(&sensor);

    AvalancheMicroscopic aval;
    aval.UseWeightingPotential();
    aval.SetSensor(&sensor);
    // aval.EnableDebugging();

    // Set time window where the calculations will be done microscopically
    const double tMaxWindow =  2; //  2ns stochastic regime?
    aval.SetTimeWindow(0., tMaxWindow);
    avalgrid.SetAvalancheMicroscopic(&aval);

    double xc = 0., yc = 0., zc = 0., tc = 0., ec = 0., extra = 0.;
    int ne = 0;
    // Retrieve the clusters along the track.
    while (track.GetCluster(xc, yc, zc, tc, ne, ec, extra)) {
        // Loop over the electrons in the cluster.
        if(debug_mode) {
            std::cout << "number of electrons in cluster: " << ne << std::endl;
        }
        for (int j = 0; j < ne; ++j) {
            double xe = 0., ye = 0., ze = 0., te = 0., ee = 0.;
            double dxe = 0., dye = 0., dze = 0.;
            track.GetElectron(j, xe, ye, ze, te, ee, dxe, dye, dze);
            // Propagate avalanche though gap within the time window set (null collision MC model (Schindler Thesis?))
            aval.AvalancheElectron(xe, ye, ze, te, ee, dxe, dye, dze);

            if(debug_mode){
                int nee, nie;
                aval.GetAvalancheSize(nee, nie);
                std::cout << "AvalancheSize after 2ns: " <<nee << std::endl;
            }
            // Stops calculation after tMaxWindow ns
            // add electrons to avalgrid instance
            avalgrid.GetElectronsFromAvalancheMicroscopic();
        }
    }

    // Start grid based avalanche calculations starting from where the microscopic calculations stopped
    // this transport mechanism uses Legler's (61) model
    avalgrid.StartGridAvalanche();

    // Raether limit exceeding -> potential streamer formation
    int aval_size = avalgrid.GetAvalancheSize();
    if(debug_mode){
        std::cout << "Avalanche Size: " << aval_size << std::endl;
    }

    if(aval_size>=  1.6e7) {
        streamer_flag = true;
    } else {
        streamer_flag = false;
    }

    // get time crossing of signal at electrode
    int nof_crossings;
    sensor.ComputeThresholdCrossings(current_threshold, label, nof_crossings);

    if(nof_crossings > 0){
        double time, level;
        bool rise;
        // get first threshold crossing
        sensor.GetThresholdCrossing(0, time, level, rise);
        t_cross = time;
    } else {
        // no crossing thus time set to negative value
        t_cross = NAN;
    }

    // integrate signal to get induced charge
    if(sensor.IntegrateSignal(label)){
        q_ind = sensor.GetSignal(label, last_bin - 1);
    } else {
        q_ind = NAN;
    }
}


int main(int argc, char * argv[]){
    bool plot_signal = true;
    bool debug_mode = false;
    int nof_events = 4;

    // In simulation, a current threshold of 1.25 µA has been chosen (Datta et al (2022))
    double current_threshold = - 1.25; // [muA]
    std::vector<double> induced_charges;
    std::vector<double> time_crossings;
    std::vector<bool> streamer_flags;

    // ROOT application, needs to be initialized otherwise it does not show any plots or canvas
    TApplication app("app", &argc, argv);
    plottingEngine.SetDefaultStyle();

    // gas medium
    MediumMagboltz gas;
    gas.LoadGasFile("ar_93_co2_7.gas");
    gas.Initialise(true);

    // The geometry of the RPC
    double gas_gap = 0.1; // [cm]
    double resistive_plate_size = 0.2; // [cm]
    std::vector<double> d = {resistive_plate_size, gas_gap, resistive_plate_size};  // [cm], z = [0, 0.2] U [0.2, 0.4] U [0.4, 0.6] style
    std::vector<double> eps = {8., 1., 8.};  // [1]
    const double voltage = -5000;  // [V]
    // resistive thus conducting electrodes (voltage on their surface): layer 1 and 3 (2 is gas phase)
    std::vector<int> sigma = {1, 3};

    auto* rpc = new ComponentParallelPlate();
    rpc->Setup(d.size(), eps, d, voltage, sigma);
    rpc->SetMedium(&gas); // how to adjust the epsilon for the gas gap?

    // box containing the gas gap (it should be y [0.2, 0.4])
    // this medium defines the gas region used in sensor
    SolidBox box(0, resistive_plate_size + gas_gap / 2, 0, 5, gas_gap / 2, 5);
    GeometrySimple geo;
    geo.AddSolid(&box, &gas);
    rpc->SetGeometry(&geo);

    if(debug_mode){
        rpc->EnableDebugging();

        double exx, eyy, ezz;
        int status;
        Medium* m;
        // y axis is drift axis
        rpc->ElectricField(0, resistive_plate_size + gas_gap / 2, 0, exx, eyy, ezz, m, status);
        std::cout << "Electric field (xyz): " << exx << " " << eyy << " " << ezz << std::endl;
    }

    // Adding a readout structure.
    const std::string label = "readout_plane";
    // adds a readout plane at the anode of the Parallel Plate (y = 0)
    rpc->AddPlane(label);

    // Create the sensor.
    Sensor sensor;
    sensor.AddComponent(rpc);
    sensor.AddElectrode(rpc, label);
    // Set the time bins.
    const unsigned int nTimeBins = 150;
    const double tmin = 0.;
    const double tmax = 25.; // typical time of signal in [ns]
    const double tstep = (tmax - tmin) / nTimeBins;
    sensor.SetTimeWindow(tmin, tstep, nTimeBins);


    // Event Creation
    for(int i = 0; i < nof_events; i++) {
        double q_ind; // induced total charge from electrons (main contribution for small time scales)
        double t_crossing; // time of crossing the current threshold (if any)
        bool streamer_flag;

        sensor.ClearSignal();
        single_passage(sensor, q_ind, t_crossing, streamer_flag, label, nTimeBins, current_threshold, d, debug_mode);

        induced_charges.push_back(q_ind);
        time_crossings.push_back(t_crossing);
        streamer_flags.push_back(streamer_flag);
        std::cout << q_ind << " " << t_crossing << " " << streamer_flag << std::endl;
    }


    // Plot of Signal on Electrodes (pay attention as it is the integrated signal!)
    if(plot_signal) {
        auto *signalView = new ViewSignal();
        auto *cSignal = new TCanvas("cSignal", "", 600, 600);
        signalView->SetCanvas(cSignal);
        signalView->SetSensor(&sensor);
        signalView->PlotSignal(label);
        cSignal->Update();
        gSystem->ProcessEvents();
        sensor.ExportSignal(label,"SignalNaN");
    }

    app.Run(kTRUE);
    return 0;
}
